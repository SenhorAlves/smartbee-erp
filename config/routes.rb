Rails.application.routes.draw do
  root 'administrative#index'

  # Nested routes para painel administrativo
  namespace :administrative do
    resources :admins
    resources :recipes
  end

  # Redirecionamento de rotas do devise para administrador
  devise_for :admins, controllers: {
    sessions: 'admins/sessions',
    passwords: 'admins/passwords',
    registrations: 'admins/registrations',
    confirmations: 'admins/confirmations'
  }

end
