source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.3'

# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'

# Use Puma as the app server
gem 'puma', '~> 3.11'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# A runtime developer console and IRB alternative with powerful introspection capabilities.
gem 'pry-rails'

# jquery
gem 'jquery-rails'

#Integrate Select2 javascript library with Rails asset pipeline https://github.com/argerim/select2-rails
gem "select2-rails"

# Mascara nos inputs
gem 'jquery_mask_rails', '~> 0.1.0'

# the font-awesome font bundled as an asset for the rails asset pipeline
gem 'font-awesome-rails'

#Integration of RubyMoney - Money with Rails
gem 'money-rails', '~>1.12'

# Flexible authentication solution for Rails with Warden.
gem 'devise'

#  Validate, generate and format CPF/CNPJ numbers. 
gem 'cpf_cnpj'

# Dynamic nested forms using jQuery made easy
gem 'cocoon'

###################### Bibliotecas de Assets(estilos/scripts) ##################################################

source 'https://rails-assets.org' do 
  gem 'rails-assets-izitoast' # Mensagens de Alerta e Confirmação - Biblioteca Completa
  gem 'rails-assets-iCheck'
  gem 'rails-assets-bootstrap'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # To rename rails application 
  gem 'rename'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
