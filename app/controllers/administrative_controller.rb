class AdministrativeController < ActionController::Base
    before_action :authenticate_admin!
    layout 'administrative'
    
    def index
    end
end
