class Recipe < ApplicationRecord
    has_many :ingredients, dependent: :destroy
    accepts_nested_attributes_for :ingredients, reject_if: ->(attributes){ attributes['name'].blank? }, allow_destroy: true

    monetize :price_cents
end
