$(document).on('turbolinks:load', () => {
    // phone 8 or 9 digits mask
    if($('.phone-mask').val() !== undefined){
        const phoneEl = $('.phone-mask');
        function fixmask() {
            if( phoneEl.val().length == 15) {
                phoneEl.mask('(00) 00000-0009', {placeholder: "(85) 91234-5678"});
            } else {
                phoneEl.mask('(00) 0000-00009', {placeholder: "(85) 91234-5678"});
            }
        };
        fixmask();
        phoneEl.blur(fixmask);
    }    
    // other masks
    $('.cpf-mask').mask('000.000.000-00', {placeholder: "___.___.___-__"});
    $('.date-mask').mask('00/00/0000', {placeholder: "dd/mm/aaaa"});
    $('.cep-mask').mask('00000-000', {placeholder: "_____-___"});
    $('.money-mask').mask('000.000.000.000,00', {placeholder: "R$ 00,00", reverse: true});
});