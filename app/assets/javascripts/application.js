//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require inspinia
//= require izitoast
//= require iCheck
//= require pace
//= require jquery.metisMenu
//= require jquery.slimscroll.js
//= require activestorage
//= require_tree .
//= require datatables
//= require jquery.mask
//= require select2
//= require turbolinks
//= require buttons.colVis.js
//= require cocoon

$(document).on('turbolinks:load', function() {

    $('.select2').select2();

    $('#side-menu').metisMenu({toggle: false });

    $.extend( $.fn.dataTable.defaults, {
        language: portuguese,
        pageLength: 10,
        dom: 'fBtpi'
    });
      
    $('.i-check').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    let dataTable = $('.datatable').DataTable({
        buttons: [
          {
            extend: 'excelHtml5',
            exportOptions: {columns: [':visible']}
          },
          {
            extend: 'pdf',
            exportOptions: {columns: [':visible']}
          },
          'colvis'
        ]
    });

    // Fix adding filtering controls twice
    document.addEventListener("turbolinks:before-cache", function() {
      if (dataTable !== null) {
        dataTable.destroy();
        dataTable = null;
      }
    });
})

// DataTables language settings here for readability
const portuguese = {
  ssEmptyTable:"Nenhum registro encontrado",
  sInfo:"Mostrando de _START_ até _END_ de _TOTAL_ registros",
  sInfoEmpty:"Mostrando 0 até 0 de 0 registros",
  sInfoFiltered:"(Filtrados de _MAX_ registros)",
  sInfoPostFix:"",
  sInfoThousands:".",
  sLengthMenu:"_MENU_ Resultados por página",
  sLoadingRecords:"Carregando...",
  sProcessing:"Processando...",
  sZeroRecords:"Nenhum registro encontrado",
  sSearch:"Pesquisar",
  oPaginate:{sNext:"Próximo",sPrevious:"Anterior",sFirst:"Primeiro",sLast:"Último"},
  oAria:{sSortAscending:": Ordenar colunas de forma ascendente",sSortDescending:": Ordenar colunas de forma descendente"},
  buttons: {colvis: 'Filtrar Colunas'}
};

