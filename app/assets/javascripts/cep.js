$(document).on('turbolinks:load', () => {
    const cepelement = $(".cep")
    cepelement.blur( () => {
        let cep, validacep
        cep = cepelement.val().replace(/\D/g, '')
        if (cep !== "") {
            validacep = /^[0-9]{8}$/
            if (validacep.test(cep)) {
                $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?")
                .done((dados) => {
                    if (!("erro" in dados)) {
                        console.log(dados)
                        $(".rua").val(dados.logradouro)
                        $(".bairro").val(dados.bairro)
                        $(".cidade").val(dados.localidade)
                        $(".uf").val(dados.uf)
                    } else 
                        iziToast.error({message: "CEP não localizado"});
                });
            } else 
                iziToast.error({message: "CEP inválido"});
        }
    });
});